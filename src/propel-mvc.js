
/*
 * Copyright 2014 Harlan Noonkester
 *
 * All rights reserved
 */

function Mvc(util) {
    var self = this;

    initExtenders();
    initBindings();

    self.applyBindings = function(model, element) {
        if (element === undefined) {
            ko.applyBindings(model);
        } else {
            ko.applyBindings(model, element);
        }
    };

    self.unwrap = function(model, element) {
        ko.unwrap(model, element);
    };

    self.clearAll = function(model) {
        for (var p in model) {
            if (model.hasOwnProperty(p)) {
                var prop = model[p];
                if (ko.isObservable(prop) && !ko.isComputed(prop)) {
                    if (util.isFunction(prop.removeAll)) {
                        // This is an observable array
                        prop.removeAll();
                    } else {
                        // Non-array observable
                        prop(null);
                    }
                }
            }
        }
    }

    self.clearExcluding = function (model, excluding) {
        if (util.isArray(excluding) == false) {
            throw new Error("mvc.clearExcluding called  withexcluding argument which is not an array");
        }
        for (var p in model) {
            if (model.hasOwnProperty(p)) {
                var prop = model[p];
                if (ko.isObservable(prop) && !ko.isComputed(prop)) {
                    if (excluding.indexOf(prop) == -1) {
                        if (util.isFunction(prop.removeAll)) {
                            // This is an observable array
                            prop.removeAll();
                        } else {
                            // Non-array observable
                            prop(null);
                        }
                    }
                }
            }
        }
    }

    self.observable = function (value) {
        if (value === undefined) {
            return ko.observable();
        } else {
            return ko.observable(value);
        }
    };

    self.observableArray = function(value) {
        if (value === undefined) {
            return ko.observableArray();
        } else {
            return ko.observableArray(value);
        }
    };

    self.computed = function(func) {
        return ko.computed(func);
    };

    self.updateModel = function(model, data, mapping) {
        mapping = (mapping) ? mapping : {};
        ko.mapping.fromJS(data, mapping, model);
        return model;
    };

    self.isObservable = function(value) {
        return ko.isObservable(value);
    }

    function initExtenders() {
        // Knockout extender which will create child observable "error", used for displaying validation errors.
        ko.extenders.error = function (target) {
            target.error = nf.mvc.observable();
        };

    }

    function initBindings() {

        ko.bindingHandlers.focusChange = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = valueAccessor();
                var focusChangeCallback;
                if (ko.isObservable(value)) {
                    // If value is observable we should have a focusChange function, added as an extension.
                    if (!util.isFunction(value.focusChange)) {
                        throw new Error("focusChange binding could not find focusChange extender function on the observable - elementId = " + element.id);
                    }
                    $(element).focus(value.focusChange);
                    $(element).blur(value.focusChange);
                } else if (util.isFunction(value)) {
                    focusChangeCallback = value;
                    $(element).focus(focusChangeCallback);
                    $(element).blur(focusChangeCallback);
                } else if (util.isUserDefinedObject(value) && util.isFunction(value.action)) {
                    var action = value.action;
                    var additionalArgs = value;
                    // Delete the action from additional args so it's not passed into action (itself)
                    delete additionalArgs.action;
                    $(element).focus(function (event) {
                        action(event, additionalArgs);
                    });
                    $(element).blur(function () {
                        action(event, additionalArgs);
                    });
                }
            }
        };

        ko.bindingHandlers.focusChangeInvoke = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                // Support focusChange: actionFunc or focusChange: {action:actionFunc, name:'someName'}
                var value = valueAccessor();
                var additionalArgs = {
                };
                var action;
                if (util.isFunction(value)) {
                    action = value;
                } else if (util.isUserDefinedObject(value) && util.isFunction(value.action)) {
                    action = value.action;
                    additionalArgs = value;
                    // Delete the action from additional args so it's not passed into action (itself)
                    delete additionalArgs.action;
                } else {
                    throw new Error("Element " + element.id + " define focusChange binding but action method is not defined.");
                }
                $(element).focus(function (event) {
                    action(event, additionalArgs);
                });
                $(element).blur(function () {
                    action(event, additionalArgs);
                });
            }
        };

        ko.bindingHandlers.date = {
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value != null) {
                    var pattern = allBindingsAccessor.datePattern || 'MM/DD/YYYY';
                    $(element).html(moment(value).format(pattern));
                }
            }
        };

        ko.bindingHandlers.phoneNumber = {
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                $(element).html(util.formatPhoneNumber(value));
            }
        };

        ko.bindingHandlers.masked = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

                //
                // Begin direct copy knockout value binding so we can modify the value from the element to use the raw value.
                //
                // Always catch "change" event; possibly other events too if asked
                var eventsToCatch = ["change"];
                var requestedEventsToCatch = allBindingsAccessor.get("valueUpdate");
                var propertyChangedFired = false;
                if (requestedEventsToCatch) {
                    if (typeof requestedEventsToCatch == "string") // Allow both individual event names, and arrays of event names
                        requestedEventsToCatch = [requestedEventsToCatch];
                    ko.utils.arrayPushAll(eventsToCatch, requestedEventsToCatch);
                    eventsToCatch = ko.utils.arrayGetDistinctValues(eventsToCatch);
                }

                var valueUpdateHandler = function () {
                    propertyChangedFired = false;
                    var modelValue = valueAccessor();

                    // Note - this is the reason we are overriding the value implementation, we want the raw value
                    // without the mask which is accessed by calling $(element).data($.mask.dataName)(). It's safe
                    // in this case not to call "ko.selectExtensions.readValue(element)" as KO does here because 
                    // we are only working with input fields here and that method extra functionality for selects.
                    var elementValue = $(element).data($.mask.dataName)();

                    // Function writeValueToProperty below was taken from private method ko.expressionRewriting.writeValueToProperty(...);
                    function writeValueToProperty(property, allBindings, key, value, checkIfDifferent) {
                        if (!property || !ko.isObservable(property)) {
                            var propWriters = allBindings.get('_ko_property_writers');
                            if (propWriters && propWriters[key])
                                propWriters[key](value);
                        } else if (ko.isWriteableObservable(property) && (!checkIfDifferent || property.peek() !== value)) {
                            property(value);
                        }
                    }

                    writeValueToProperty(modelValue, allBindingsAccessor, 'value', elementValue);
                }

                // Workaround for https://github.com/SteveSanderson/knockout/issues/122
                // IE doesn't fire "change" events on textboxes if the user selects a value from its autocomplete list
                var ieAutoCompleteHackNeeded = ko.utils.ieVersion && element.tagName.toLowerCase() == "input" && element.type == "text"
                                               && element.autocomplete != "off" && (!element.form || element.form.autocomplete != "off");
                if (ieAutoCompleteHackNeeded && ko.utils.arrayIndexOf(eventsToCatch, "propertychange") == -1) {
                    ko.utils.registerEventHandler(element, "propertychange", function () { propertyChangedFired = true });
                    ko.utils.registerEventHandler(element, "focus", function () { propertyChangedFired = false });
                    ko.utils.registerEventHandler(element, "blur", function () {
                        if (propertyChangedFired) {
                            valueUpdateHandler();
                        }
                    });
                }

                ko.utils.arrayForEach(eventsToCatch, function (eventName) {
                    // The syntax "after<eventname>" means "run the handler asynchronously after the event"
                    // This is useful, for example, to catch "keydown" events after the browser has updated the control
                    // (otherwise, ko.selectExtensions.readValue(this) will receive the control's value *before* the key event)
                    var handler = valueUpdateHandler;
                    if (util.startsWith(eventName, "after")) {
                        handler = function () { setTimeout(valueUpdateHandler, 0); };
                        eventName = eventName.substring("after".length);
                    }
                    ko.utils.registerEventHandler(element, eventName, handler);
                });
                //
                // End copy of knockout value binding
                //

                // Find the mask in maskedOtions binding
                var mask;
                var options = {};
                if (allBindingsAccessor().maskedOptions) {
                    var maskedOptions = allBindingsAccessor().maskedOptions;
                    if (util.isString(maskedOptions)) {
                        // If only a string value for maskedOptions then use that as the mask
                        mask = maskedOptions;
                    } else if (util.isUserDefinedObject(maskedOptions)) {
                        // If an object then look for a mask property
                        mask = maskedOptions.mask;
                        // And use the rest as options.
                        options = maskedOptions;
                        delete options.mask;
                    }
                }
                if (mask) {
                    if (!options.placeholder) {
                        // Default to space as placeholder if not specified.
                        options.placeholder = " ";
                    }
                    // Apply mask using jquery.maskedinput
                    $(element).mask(mask, options);
                } else {
                    throw Error("Element " + element.id + "bound with mask yet does not define a mask.");
                }

            },
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                // Update the value using standard value binding.
                ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor, viewModel);

                // Trigger a blur.mask event so that jquery.maskedinput will invoke checkVal
                $(element).trigger("blur.mask");
            }
        };

        ko.bindingHandlers.datepicker = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                //initialize datepicker with some optional options
                var options = allBindingsAccessor().datepickerOptions || {};
                $(element).datepicker(options);

                //handle the field changing
                ko.utils.registerEventHandler(element, "change", function () {
                    var observable = valueAccessor();
                    observable($(element).datepicker("getDate"));
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                    $(element).datepicker("destroy");
                });

            },
            //update the control when the view model changes
            update: function(element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value != null) {
                    value = moment(value).toDate();
                }
                $(element).datepicker("setDate", value);
            }
        };

        ko.bindingHandlers.jqDialog = {
            init: function (element, valueAccessor) {
                var options = ko.utils.unwrapObservable(valueAccessor()) || {};
                options.autoOpen = false;
                options.resizable = false;
                options.width = 'auto';
                options.height = 'auto';
                options.width = 'auto';
                options.modal = true;

                // Dispose of dialog
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).dialog("destroy");
                });
                $(element).dialog(options);
            }
        };
        ko.bindingHandlers.openDialog = {
            update: function (element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value) {
                    $(element).dialog("open");
                } else {
                    $(element).dialog("close");
                }
            }
        };
        ko.bindingHandlers.jqButton = {
            init: function (element, valueAccessor) {
                var options = ko.utils.unwrapObservable(valueAccessor()) || {};

                // Disposal
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).button("destroy");
                });
                $(element).button(options);
            }
        };

        ko.bindingHandlers.money = {
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value != null) {
                    var formattedValue = "$" + value;
                    $(element).html(formattedValue);
                }
            }
        };

        ko.bindingHandlers.spinner = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                //initialize spinner with some optional options
                var options = allBindingsAccessor().spinnerOptions || {};
                $(element).spinner(options);

                //handle the field changing
                ko.utils.registerEventHandler(element, "spinchange", function () {
                    var val = $(element).spinner("value");
                    var valid = true;
                    if (options.min != null && val < options.min) {
                        valid = false;
                    }
                    if (options.max != null && val > options.max) {
                        valid = false;
                    }
                    if (valid) {
                        var observable = valueAccessor();
                        observable($(element).spinner("value"));
                    } else {

                        var lastValue = ko.utils.unwrapObservable(valueAccessor());
                        $(element).spinner("value", lastValue);
                    }
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).spinner("destroy");
                });

            },
            update: function (element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());

                current = $(element).spinner("value");
                if (value !== current) {
                    $(element).spinner("value", value);
                }
            }
        };

    }
}

