
/*
 * Copyright 2014 Harlan Noonkester
 *
 * All rights reserved
 */

function Ajax(util, baseUrl) {
    if (!util) {
        throw new Error("Null util argument given to Ajax constructor.");
    }
    if (!baseUrl) {
        throw new Error("Null baseUrl argument given to Ajax constructor.");
    }
    var self = this;
    self.baseUrl = baseUrl;

    // Default OnAjaxError
    var defaultAjaxErrorHandler = function (xhr) {
        alert("Ajax failure - " + xhr.status + " : " + xhr.statusText);
        return xhr;
    };
    self.setDefaultAjaxErrorHandler = function (handler) {
        if (!util.isFunction(handler)) {
            throw new Error("Argument to setDefaultAjaxErrorHandler is not a function");
        }
        defaultAjaxErrorHandler = handler;
        return this;
    };

    // Default settings used for ajax (jQuery ajax settings argument).
    var defaultAjaxSettings = {};
    self.setDefaultAjaxSettings = function (onError) {
        if (!util.isUserDefinedObject(onError)) {
            throw new Error("setDefaultAjaxSettings argument is not an object");
        }
        defaultAjaxSettings = onError;
        return this;
    };

    // REVIEW - should likely do this elsewhere, but we have all the needed pieces here.
    // Add catchAjax method to Promise. Arguments are the function to call when the error
    // has occurred (optional, defaults to default ajax error handler) and also optionally
    // an http status code (or list of) that should be used in the predicate (determining 
    // if the function is called for the given ajax error).
    Promise.prototype.catchAjax = function (arg1, arg2) {
        var onError = defaultAjaxErrorHandler;
        var statusCodes = null;
        if (arg1) {
            if (util.isFunction(arg1)) {
                onError = arg1;
            } else {
                statusCodes = arg1;
            }
        }
        if (arg2) {
            if (util.isFunction(arg2)) {
                onError = arg2;
            } else if (statusCodes == null) {
                statusCodes = arg2;
            }
        }

        // The predicate will ensure it's an ajax request error and that any specified status code will be taken into consideration.
        var predicate = new AjaxErrorPredicate(util, statusCodes);

        // Add a catch to "this" (assummed to be the promise) using the predicate and the onError function.
        return this.catch(predicate.isTrue, onError);
    };

    // Methods
    self.get = function () {
        var url = util.buildUrlFromArgs(self.baseUrl, arguments);
        var request = new AjaxRequest(util, url, defaultAjaxErrorHandler, defaultAjaxSettings);
        request.processData(true);
        request.type("GET");
        return request;
    };
    self.post = function () {
        var url = util.buildUrlFromArgs(self.baseUrl, arguments);
        var request = new AjaxRequest(util, url, defaultAjaxErrorHandler, defaultAjaxSettings);
        request.type("POST");
        return request;
    };
    self.put = function () {
        var url = util.buildUrlFromArgs(self.baseUrl, arguments);
        var request = new AjaxRequest(util, url, defaultAjaxErrorHandler, defaultAjaxSettings);
        request.type("PUT");
        return request;
    };
    self.delete = function () {
        var url = util.buildUrlFromArgs(self.baseUrl, arguments);
        var request = new AjaxRequest(util, url, defaultAjaxErrorHandler, defaultAjaxSettings);
        request.type("DELETE");
        return request;
    };

}

function AjaxRequest(util, url, defaultAjaxOnError, ajaxSettingsArg) {
    var self = this;
    if (!util) {
        throw new Error("Web constructed without util.");
    }

    // Clone the setting and callback args so we don't modify defaults.
    self.ajaxSettings = $.extend(true, {}, ajaxSettingsArg);

    // Execute
    self.execute = function () {
        self.ajaxSettings.url = url;
        var xhr = $.ajax(self.ajaxSettings);
        // Add the url as a field to help with error handling.
        xhr.requestUrl = url;

        // Resolve to a promise
        return Promise.resolve(xhr);
    };

    // Data
    self.data = function (data, contentType) {
        self.ajaxSettings.data = data;
        if (contentType) {
            self.ajaxSettings.contentType = contentType;
        }
        return this;
    };
    self.jsonData = function (data) {
        self.ajaxSettings.data = data;
        self.ajaxSettings.contentType = "application/json";
        return this;
    };
    self.jsonObject = function (object) {
        self.ajaxSettings.data = util.toJson(object);
        self.ajaxSettings.contentType = "application/json";
        return this;
    };

    // ajaxSettings
    self.beforeSend = function () {
        self.ajaxSettings.beforeSend = arguments;
        return self;
    };
    self.type = function(type) {
        self.ajaxSettings.type = type;
        return this;
    };

    // Expected/requested data format specified by object in form {xml: 'text/xml',text: 'text/plain'}
    self.accepts = function (accepts) {
        if (util.isUserDefinedObject(accepts)) {
            throw new Error("Ajax.accepts() called with argument which is not an object, jQuery doc is not so clear on this but that is what's expected. May want to use dataType.");
        }
        self.ajaxSettings.accepts = accepts;
        return this;
    };

    // Expected/requested data format specified by string in form "application/json"
    self.dataType = function (dataType) {
        self.ajaxSettings.dataType = dataType;
        return this;
    };

    // If true data will be transformed to a query string, false data will be sent in content
    self.processData = function (processData) {
        self.ajaxSettings.processData = processData;
        return this;
    };

    // Content type of data being sent.
    self.contentType = function (contentType) {
        self.ajaxSettings.contentType = contentType;
        return this;
    };

    self.statusCodeCallbacks = function (statusCodeCallbacks) {
        self.ajaxSettings.statusCode = statusCodeCallbacks;
        return this;
    };
    self.headers = function (headers) {
        self.ajaxSettings.headers = headers;
        return this;
    };
    self.timeout = function (timeout) {
        self.ajaxSettings.timeout = timeout;
        return this;
    };
    self.credentials = function (username, password) {
        self.ajaxSettings.username = username;
        self.ajaxSettings.password = password;
        return this;
    };
}

function AjaxErrorPredicate(util, statusCodes) {
    var self = this;
    var codes = [];

    // Status codes may be an array or single value.
    if (statusCodes != null) {
        if (util.isArray(statusCodes)) {
            codes = statusCodes;
        } else {
            codes.push(statusCodes);
        }
    }

    // Return true if error is an XHR and meets any statusCode requirements.
    self.isTrue = function (error) {
        // A simple to check to see if it looks like an jqXHR
        if (error.status == undefined || error.statusCode == undefined) {
            // Definitely not an jqXHR, so return false (don't give to this handler).
            return false;
        }
        if (codes.length == 0) {
            return true;
        }
        for (var i=0; i<codes.length; i++) {
            if (error.status == codes[i]) {
                return true;
            }
        }
        return false;
    };
}


/*
 * Copyright 2014 Harlan Noonkester
 *
 * All rights reserved
 */

function DialogFactory(mvc, contentBaseUrl, dialogRootAppendSelector) {
    var self = this;

    // Establish patterns used for dialog creation (these can be overriden after construction).
    self.contentUrlPattern = "{contentBaseUrl}/{dialogName}-dialog.html";
    self.dialogRootIdPattern = "{dialogName}-root";
    self.dialogRootHtmlPattern = "<div id='{dialogRootId}' class='modal fade' tabindex='-1' role='dialog'></div>";
    self.dialogContentIdPattern = "{dialogName}-dialog";

    // Bootstrap modal options
    self.dialogOptions = {};

    self.createDialog = function (dialogName, dialogOptionsForThisDialog) {
        var contentUrl = self.contentUrlPattern
            .replace(/{contentBaseUrl}/gi, contentBaseUrl)
            .replace(/{dialogName}/gi, dialogName);
        var dialogRootId = self.dialogRootIdPattern
            .replace(/{dialogName}/gi, dialogName);
        var dialogContentId = self.dialogContentIdPattern
            .replace(/{dialogName}/gi, dialogName);

        // Create the dialog root html and append (this is the root html element the actual dialog will be appended to).
        var dialogRootHtml = self.dialogRootHtmlPattern
            .replace(/{dialogRootId}/gi, dialogRootId);
        $(dialogRootAppendSelector).append(dialogRootHtml);

        dialogOptionsForThisDialog = (dialogOptionsForThisDialog != null) ? dialogOptionsForThisDialog : self.dialogOptions; 

        // Create the dialog
        return new Dialog(mvc, {
            contentUrl: contentUrl,
            dialogRootId: dialogRootId,
            dialogContentId: dialogContentId,
            dialogOptions: dialogOptionsForThisDialog
        });
    }
}

function Dialog(mvc, init) {
    var self = this;
    init = (init != null) ? init : {};
    // The url where the content for the dialog can be loaded.
    self.contentUrl = init.contentUrl;
    // The selector for the dialog root element.
    self.dialogRootSelector = "#" + init.dialogRootId;
    // The selector for the dialog content
    self.dialogContentSelector = "#" + init.dialogContentId;
    // Bootstrap modal options
    self.dialogOptions = init.dialogOptions;

    // Show the dialog, binding the given viewModel (callback is optional)
    self.show = function (viewModel) {
        return new Promise(function (resolve, reject) {
            $(self.dialogRootSelector).load(self.contentUrl, function () {
                if (viewModel) {
                    mvc.applyBindings(viewModel, $(self.dialogContentSelector).get(0));
                }
                $(self.dialogRootSelector).modal(self.dialogOptions);

                // Once the model is shown we do the following.
                $(self.dialogRootSelector).on('shown.bs.modal', function () {
                    // Auto focus (bootstrap seems to have issues with this on it's own so we do it here).
                    $(self.dialogRootSelector + " [autofocus]").focus();

                    // Resolve the promise.
                    resolve();
                });

            });

        });
    };

    // Hide the dialog
    self.hide = function () {
        $(self.dialogRootSelector).modal('hide');
    }
};


/*
 * Copyright 2014 Harlan Noonkester
 *
 * All rights reserved
 */

function EventBus(util, eventTypes) {
    var self = this;
    var subscriptions = [];

    // Auto assigns values of event type properties to their property name.
    for (var type in eventTypes) {
        if (eventTypes.hasOwnProperty(type)) {
            eventTypes[type] = type;
        }
    }

    self.hasSubscription = function(checkSubscription) {
        for (var i=0; i<subscriptions.length; i++) {
            if (subscriptions[i] === checkSubscription) {
                return true;
            }
        }
        return false;
    };

    self.subscriptionCount = function() {
        return subscriptions.length;
    };

    self.hasEventType = function(eventType) {
        if (!eventType) {
            return false;
        } else {
            return eventTypes.hasOwnProperty(eventType.toUpperCase());
        }
    };

    self.subscribe = function(subscribeTo, callback) {
        var subscription = new EventSubscription(subscribeTo, callback);
        subscriptions.push(subscription);
        return subscription;
    };

    self.unsubscribe = function(subscription) {
        var index = subscriptions.indexOf(subscription);
        if (index >= 0) {
            subscriptions.splice(index, 1);
        }
    };

    self.publish = function(eventType, data) {
        eventType = eventType.toUpperCase();
        if (!eventTypes.hasOwnProperty(eventType)) {
            throw Error("Attempting to publish with an undefined event type " + eventType);
        }
        for (var i=0; i<subscriptions.length; i++) {
            var subscription = subscriptions[i];
            // Data is first argument to subscriber
            subscription.handle(data, eventType);
        }
    };

    function EventSubscription(subscribedEventTypes, callback) {
        var self = this;
        if (!subscribedEventTypes) {
            throw Error("Attempting to subscribe with null event type.");
        }

        // Maintain as an array of event types.
        if (util.isArray(subscribedEventTypes) == false) {
            subscribedEventTypes = [subscribedEventTypes];
        }

        // Ensure all subscribedEventTypes exist.
        for (var i=0; i<subscribedEventTypes.length; i++) {
            var eventType = subscribedEventTypes[i];
            if (!eventTypes.hasOwnProperty(eventType.toUpperCase())) {
                throw Error("Attempting to subscribe to undefined event type " + eventType);
            }
        }

        // Data is first argument because in most cases that will be the only argument, 
        // subscriber will not care about the eventType.
        self.handle = function(data, eventType) {
            for (var i=0; i<subscribedEventTypes.length; i++) {
                var subscribedEventType = subscribedEventTypes[i];
                if (subscribedEventType === eventType) {
                    callback(data, eventType);
                    break;
                }
            }
        }
    }
}

/*
 * Copyright 2014 Harlan Noonkester
 *
 * All rights reserved
 */

function Mvc(util) {
    var self = this;

    initExtenders();
    initBindings();

    self.applyBindings = function(model, element) {
        if (element === undefined) {
            ko.applyBindings(model);
        } else {
            ko.applyBindings(model, element);
        }
    };

    self.unwrap = function(model, element) {
        ko.unwrap(model, element);
    };

    self.clearAll = function(model) {
        for (var p in model) {
            if (model.hasOwnProperty(p)) {
                var prop = model[p];
                if (ko.isObservable(prop) && !ko.isComputed(prop)) {
                    if (util.isFunction(prop.removeAll)) {
                        // This is an observable array
                        prop.removeAll();
                    } else {
                        // Non-array observable
                        prop(null);
                    }
                }
            }
        }
    }

    self.clearExcluding = function (model, excluding) {
        if (util.isArray(excluding) == false) {
            throw new Error("mvc.clearExcluding called  withexcluding argument which is not an array");
        }
        for (var p in model) {
            if (model.hasOwnProperty(p)) {
                var prop = model[p];
                if (ko.isObservable(prop) && !ko.isComputed(prop)) {
                    if (excluding.indexOf(prop) == -1) {
                        if (util.isFunction(prop.removeAll)) {
                            // This is an observable array
                            prop.removeAll();
                        } else {
                            // Non-array observable
                            prop(null);
                        }
                    }
                }
            }
        }
    }

    self.observable = function (value) {
        if (value === undefined) {
            return ko.observable();
        } else {
            return ko.observable(value);
        }
    };

    self.observableArray = function(value) {
        if (value === undefined) {
            return ko.observableArray();
        } else {
            return ko.observableArray(value);
        }
    };

    self.computed = function(func) {
        return ko.computed(func);
    };

    self.updateModel = function(model, data, mapping) {
        mapping = (mapping) ? mapping : {};
        ko.mapping.fromJS(data, mapping, model);
        return model;
    };

    self.isObservable = function(value) {
        return ko.isObservable(value);
    }

    function initExtenders() {
        // Knockout extender which will create child observable "error", used for displaying validation errors.
        ko.extenders.error = function (target) {
            target.error = nf.mvc.observable();
        };

    }

    function initBindings() {

        ko.bindingHandlers.focusChange = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = valueAccessor();
                var focusChangeCallback;
                if (ko.isObservable(value)) {
                    // If value is observable we should have a focusChange function, added as an extension.
                    if (!util.isFunction(value.focusChange)) {
                        throw new Error("focusChange binding could not find focusChange extender function on the observable - elementId = " + element.id);
                    }
                    $(element).focus(value.focusChange);
                    $(element).blur(value.focusChange);
                } else if (util.isFunction(value)) {
                    focusChangeCallback = value;
                    $(element).focus(focusChangeCallback);
                    $(element).blur(focusChangeCallback);
                } else if (util.isUserDefinedObject(value) && util.isFunction(value.action)) {
                    var action = value.action;
                    var additionalArgs = value;
                    // Delete the action from additional args so it's not passed into action (itself)
                    delete additionalArgs.action;
                    $(element).focus(function (event) {
                        action(event, additionalArgs);
                    });
                    $(element).blur(function () {
                        action(event, additionalArgs);
                    });
                }
            }
        };

        ko.bindingHandlers.focusChangeInvoke = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                // Support focusChange: actionFunc or focusChange: {action:actionFunc, name:'someName'}
                var value = valueAccessor();
                var additionalArgs = {
                };
                var action;
                if (util.isFunction(value)) {
                    action = value;
                } else if (util.isUserDefinedObject(value) && util.isFunction(value.action)) {
                    action = value.action;
                    additionalArgs = value;
                    // Delete the action from additional args so it's not passed into action (itself)
                    delete additionalArgs.action;
                } else {
                    throw new Error("Element " + element.id + " define focusChange binding but action method is not defined.");
                }
                $(element).focus(function (event) {
                    action(event, additionalArgs);
                });
                $(element).blur(function () {
                    action(event, additionalArgs);
                });
            }
        };

        ko.bindingHandlers.date = {
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value != null) {
                    var pattern = allBindingsAccessor.datePattern || 'MM/DD/YYYY';
                    $(element).html(moment(value).format(pattern));
                }
            }
        };

        ko.bindingHandlers.phoneNumber = {
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                $(element).html(util.formatPhoneNumber(value));
            }
        };

        ko.bindingHandlers.masked = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

                //
                // Begin direct copy knockout value binding so we can modify the value from the element to use the raw value.
                //
                // Always catch "change" event; possibly other events too if asked
                var eventsToCatch = ["change"];
                var requestedEventsToCatch = allBindingsAccessor.get("valueUpdate");
                var propertyChangedFired = false;
                if (requestedEventsToCatch) {
                    if (typeof requestedEventsToCatch == "string") // Allow both individual event names, and arrays of event names
                        requestedEventsToCatch = [requestedEventsToCatch];
                    ko.utils.arrayPushAll(eventsToCatch, requestedEventsToCatch);
                    eventsToCatch = ko.utils.arrayGetDistinctValues(eventsToCatch);
                }

                var valueUpdateHandler = function () {
                    propertyChangedFired = false;
                    var modelValue = valueAccessor();

                    // Note - this is the reason we are overriding the value implementation, we want the raw value
                    // without the mask which is accessed by calling $(element).data($.mask.dataName)(). It's safe
                    // in this case not to call "ko.selectExtensions.readValue(element)" as KO does here because 
                    // we are only working with input fields here and that method extra functionality for selects.
                    var elementValue = $(element).data($.mask.dataName)();

                    // Function writeValueToProperty below was taken from private method ko.expressionRewriting.writeValueToProperty(...);
                    function writeValueToProperty(property, allBindings, key, value, checkIfDifferent) {
                        if (!property || !ko.isObservable(property)) {
                            var propWriters = allBindings.get('_ko_property_writers');
                            if (propWriters && propWriters[key])
                                propWriters[key](value);
                        } else if (ko.isWriteableObservable(property) && (!checkIfDifferent || property.peek() !== value)) {
                            property(value);
                        }
                    }

                    writeValueToProperty(modelValue, allBindingsAccessor, 'value', elementValue);
                }

                // Workaround for https://github.com/SteveSanderson/knockout/issues/122
                // IE doesn't fire "change" events on textboxes if the user selects a value from its autocomplete list
                var ieAutoCompleteHackNeeded = ko.utils.ieVersion && element.tagName.toLowerCase() == "input" && element.type == "text"
                                               && element.autocomplete != "off" && (!element.form || element.form.autocomplete != "off");
                if (ieAutoCompleteHackNeeded && ko.utils.arrayIndexOf(eventsToCatch, "propertychange") == -1) {
                    ko.utils.registerEventHandler(element, "propertychange", function () { propertyChangedFired = true });
                    ko.utils.registerEventHandler(element, "focus", function () { propertyChangedFired = false });
                    ko.utils.registerEventHandler(element, "blur", function () {
                        if (propertyChangedFired) {
                            valueUpdateHandler();
                        }
                    });
                }

                ko.utils.arrayForEach(eventsToCatch, function (eventName) {
                    // The syntax "after<eventname>" means "run the handler asynchronously after the event"
                    // This is useful, for example, to catch "keydown" events after the browser has updated the control
                    // (otherwise, ko.selectExtensions.readValue(this) will receive the control's value *before* the key event)
                    var handler = valueUpdateHandler;
                    if (util.startsWith(eventName, "after")) {
                        handler = function () { setTimeout(valueUpdateHandler, 0); };
                        eventName = eventName.substring("after".length);
                    }
                    ko.utils.registerEventHandler(element, eventName, handler);
                });
                //
                // End copy of knockout value binding
                //

                // Find the mask in maskedOtions binding
                var mask;
                var options = {};
                if (allBindingsAccessor().maskedOptions) {
                    var maskedOptions = allBindingsAccessor().maskedOptions;
                    if (util.isString(maskedOptions)) {
                        // If only a string value for maskedOptions then use that as the mask
                        mask = maskedOptions;
                    } else if (util.isUserDefinedObject(maskedOptions)) {
                        // If an object then look for a mask property
                        mask = maskedOptions.mask;
                        // And use the rest as options.
                        options = maskedOptions;
                        delete options.mask;
                    }
                }
                if (mask) {
                    if (!options.placeholder) {
                        // Default to space as placeholder if not specified.
                        options.placeholder = " ";
                    }
                    // Apply mask using jquery.maskedinput
                    $(element).mask(mask, options);
                } else {
                    throw Error("Element " + element.id + "bound with mask yet does not define a mask.");
                }

            },
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                // Update the value using standard value binding.
                ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor, viewModel);

                // Trigger a blur.mask event so that jquery.maskedinput will invoke checkVal
                $(element).trigger("blur.mask");
            }
        };

        ko.bindingHandlers.datepicker = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                //initialize datepicker with some optional options
                var options = allBindingsAccessor().datepickerOptions || {};
                $(element).datepicker(options);

                //handle the field changing
                ko.utils.registerEventHandler(element, "change", function () {
                    var observable = valueAccessor();
                    observable($(element).datepicker("getDate"));
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                    $(element).datepicker("destroy");
                });

            },
            //update the control when the view model changes
            update: function(element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value != null) {
                    value = moment(value).toDate();
                }
                $(element).datepicker("setDate", value);
            }
        };

        ko.bindingHandlers.jqDialog = {
            init: function (element, valueAccessor) {
                var options = ko.utils.unwrapObservable(valueAccessor()) || {};
                options.autoOpen = false;
                options.resizable = false;
                options.width = 'auto';
                options.height = 'auto';
                options.width = 'auto';
                options.modal = true;

                // Dispose of dialog
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).dialog("destroy");
                });
                $(element).dialog(options);
            }
        };
        ko.bindingHandlers.openDialog = {
            update: function (element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value) {
                    $(element).dialog("open");
                } else {
                    $(element).dialog("close");
                }
            }
        };
        ko.bindingHandlers.jqButton = {
            init: function (element, valueAccessor) {
                var options = ko.utils.unwrapObservable(valueAccessor()) || {};

                // Disposal
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).button("destroy");
                });
                $(element).button(options);
            }
        };

        ko.bindingHandlers.money = {
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                if (value != null) {
                    var formattedValue = "$" + value;
                    $(element).html(formattedValue);
                }
            }
        };

        ko.bindingHandlers.spinner = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                //initialize spinner with some optional options
                var options = allBindingsAccessor().spinnerOptions || {};
                $(element).spinner(options);

                //handle the field changing
                ko.utils.registerEventHandler(element, "spinchange", function () {
                    var val = $(element).spinner("value");
                    var valid = true;
                    if (options.min != null && val < options.min) {
                        valid = false;
                    }
                    if (options.max != null && val > options.max) {
                        valid = false;
                    }
                    if (valid) {
                        var observable = valueAccessor();
                        observable($(element).spinner("value"));
                    } else {

                        var lastValue = ko.utils.unwrapObservable(valueAccessor());
                        $(element).spinner("value", lastValue);
                    }
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).spinner("destroy");
                });

            },
            update: function (element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());

                current = $(element).spinner("value");
                if (value !== current) {
                    $(element).spinner("value", value);
                }
            }
        };

    }
}



/*
 * Copyright 2014 Harlan Noonkester
 *
 * All rights reserved
 */

function Util() {
    var self = this;

    self.cloneUsingJson = function(object)
    {
        return JSON.parse(JSON.stringify(object));
    }

    self.consoleLog = function (msg) {
        if (console) {
            console.log(msg);
        } else {
            alert("Yo dog, no console! " + msg);
        }
    };

    // TODO - ideally would support other formats and 7 digit numbers
    self.formatPhoneNumber = function (phoneNumber)
    {
        if (phoneNumber == null || phoneNumber.length != 10) {
            return phoneNumber;
        }
        // TODO - need to eval what this line is doing.
        phoneNumber = phoneNumber.replace(/[^0-9]/g, '');
        phoneNumber = phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        return phoneNumber;
    }
    
    self.firstCharToUpper = function (value) {
        if (self.isString(value)) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        } else {
            return value;
        }
    }

    self.firstCharToLower = function (value) {
        if (self.isString(value)) {
            return value.charAt(0).toLowerCase() + value.slice(1);
        } else {
            return value;
        }
    }

    self.startsWith = function(value, prefix) {
        var str;
        if (!value || !prefix) {
            return false;
        }
        str = value.toString();
        return str.toString().indexOf(prefix) !== -1;
    }

    self.endsWith = function (value, suffix) {
        var str;
        if (!value || !suffix) {
            return false;
        }
        str = value.toString();
        return str.toString().indexOf(suffix, str.length - suffix.length) !== -1;
    }

    self.removeTrailing = function(value, remove) {
        if (!value || !remove) {
            return value;
        }
        var hasTrailing = value.indexOf(remove, value.length - remove.length) !== -1;
        if (hasTrailing) {
            value = value.substring(0, value.length - remove.length);
        }
        return value;
    }

    self.isUndefined = function(obj) {
        return typeof obj === "undefined";
    };

    self.isUserDefinedObject = function (obj) {
        return Object.prototype.toString.call(obj) == "[object Object]";
    };

    self.isFunction = function (obj) {
        return Object.prototype.toString.call(obj) == "[object Function]";
    };

    self.isString = function(obj) {
        return Object.prototype.toString.call(obj) == "[object String]";
    };

    self.isArray = function(obj) {
        return Object.prototype.toString.call(obj) == "[object Array]";
    };

    self.isDate = function(obj) {
        return Object.prototype.toString.call(obj) == "[object Date]";
    };

    self.toJson = function (obj) {
        return JSON.stringify(obj);
    }

    self.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };

    self.printElement = function (elementId) {
        var elem = document.getElementById(elementId);
        var domClone = elem.cloneNode(true);

        var printSection = document.getElementById("only-print-this-section");

        if (!printSection) {
            printSection = document.createElement("div");
            printSection.id = "only-print-this-section";
            document.body.appendChild(printSection);
        }

        printSection.innerHTML = "";
        printSection.appendChild(domClone);
        window.print();
    }
    
    self.hostUrl = function () {
        var hostUrl = window.location.protocol + "//" + window.location.host;
        return hostUrl;
    };

    self.appUrl = function () {
        var appUrl = window.location.protocol + "//" + window.location.host;
        var paths = window.location.pathname.split('/');
        if (paths.length > 1) {
            appUrl += "/" + paths[1];
        }
        return appUrl;
    };

    // Build a URL from the given array of path and query parameter object. The paths array must contain one
    // path at a minimum, the queryParameter argument is optional.
    self.buildUrl = function (paths, queryParamStruct) {
        if (!paths || paths.length == 0) {
            throw new Error("No path given, cannot build URL.");
        }

        // Use first path element as our base.
        var url = paths[0];
        if (self.endsWith(url, "/")) {
            // Remove any trailing /
            url = url.substring(0, url.length - 1);
        }

        // Add any additional path elements (starting at index 1).
        for (var i = 1; i < paths.length; i++) {
            var path = paths[i];
            if (!path || path.length == 0) {
                // Error if path argument is null or empty.
                throw Error("Cannot build URL, path argument is empty, url so far: " + url);
            }
            url += "/" + path;
        }

        if (queryParamStruct) {
            url += "?";
            for (var pName in queryParamStruct) {
                var pValue = queryParamStruct[pName];
                url += pName;
                url += "=";
                if (pValue != null) {
                    url += pValue;
                }
                url += "&";
            }
            // Need to remove last char from queryParams since we are adding ahead
            url = url.substring(0, url.length - 1);
        }

        return url;
    }

    // Builds a URL using a baseUrl and set of args (a frequent use case for other classes, i.e. Ajax,
    // but not often used directly in typical client). The args are examined, any string is treated as
    // a path and any object is treated as query parameter structure.
    self.buildUrlFromArgs = function (baseUrl, args) {
        var paths = [baseUrl];
        var queryParamStruct = null;

        // Assign path elements and queryParamStruct as they are found in the argument.s
        for (var i = 0; i < args.length; i++) {
            var arg = args[i];
            if (self.isUserDefinedObject(arg)) {
                if (queryParamStruct != null) {
                    throw new Error("Multiple objects in url args, only one can be provided which will be used as the query param struct");
                } else {
                    queryParamStruct = arg;
                }
            } else {
                paths.push(arg);
            }
        }

        return self.buildUrl(paths, queryParamStruct);
    }
}

function UrlBuilder(util) {
    var self = this;
    var paths = [];
    var queryParamStruct = null;

    // Add path elements, each argument added to the path.
    self.path = function() {
        for(var i=0; i<arguments.length; i++) {
            var p = arguments[i];
            if (!p || p.length == 0) {
                // Error if path argument is null or empty.
                throw Error("Path argument is empty, path so far: " + path);
            }
            paths.push(p);
        }
        return self;
    };

    // Add a query parameter
    self.param = function(pName, pVal) {
        if (!pName || pName.length == 0) {
            throw Error("Param name is undefined or empty.");
        } else {
            queryParamStruct[pName] = pVal;
        }
        return self;
    };

    self.build = function (args) {
        for (var i = 0; i < args.length; i++) {
            var arg = args[i];
            if (util.isUserDefinedObject(arg)) {
                if (queryParamStruct != null) {
                    // TODO - just need to merge the objects for this to work.
                    throw new Error("Query parameters specified by param method in build, currently only support specify query parameters in one of those ways.");
                } else {
                    queryParamStruct = arg;
                }
            } else {
                // We will check for null or empty later in buildUrl.
                paths.push(arg);
            }
        }

        // Must have atleast one path.
        if (paths.length == 0) {
            throw new Error("Cannot build URL, no path given");
        }

        // Check for a single absolute path
        return util.buildUrl(paths, queryParamStruct);

    }

}

