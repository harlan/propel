var gulp = require('gulp');
var ngAnnotate = require('gulp-ng-annotate');
var del = require('delete');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var path = require('path');
var rename = require('gulp-rename');

gulp.task('default', [
'build'
]);

gulp.task('clean', function(done){
  del('build', done);
});

gulp.task('build', [
'app-scripts'
]);

gulp.task('app-scripts', function(done){
  gulp
  .src([
    './src/propel-ajax.js',
    './src/propel-dialog.js',
    './src/propel-event-bus.js',
    './src/propel-mvc.js',
    './src/propel-util.js'
    ])
    .pipe(
      ngAnnotate({
        remove : true,
        add : true,
        single_quotes : true
      })
    )
    .pipe(concat('propel.js'))
    .pipe(gulp.dest('./dist'))
    .pipe(uglify({}))
    .pipe(rename( { extname : '.min.js' } ))
    .pipe(gulp.dest('./dist'))
    .on('end', done);
  });
