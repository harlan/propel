Propel
======

Helper of other frameworks

## Running the Code

This required NodeJS and NPM to be installed.

You will also need to make sure to install `gulp` globally through NPM:

```
npm install -g gulp bower
```

To install the project dependencies, use:

```
npm install
```

To just build project (places in `/build`), use the `build` task:

```
gulp build
```
